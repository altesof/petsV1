{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div id="aeo_cmsservice2" class="block">
<div class="container">
{if $aeocmsserviceinfos2.text != ""}
		{$aeocmsserviceinfos2.text nofilter}
{else}
    <h1>¿Cómo funciona?</h1>
    <div class="row">
        <div class="col-md-3 aeo-service2-item first">
            <h1>Llena tu formulario</h1>
            <p>Llena el formulario para que nuestro algoritmo inteligente pueda crear una dieta personalizada para tu perro.</p>
        </div>
        <div class="col-md-3 aeo-service2-item second">
            <h1>Preparamos la comida</h1>
            <p>Nuestras recetas son elaboradas con ingredientes de grado humano y cocinadas bajo las temperaturas ideales para ayudar a preservar nutrientes.</p>
        </div>
        <div class="col-md-3 aeo-service2-item third">
            <h1>Entregamos a domicilio</h1>
            <p>Recibe la comida congelada, ya cocida en paquetes individuales con la porción diaria que requiere tu perro y guárdalos en el congelador.</p>
        </div>
            <div class="col-md-3 aeo-service2-item fourth">
            <h1>Descongela y sirve</h1>
            <p>Tú y tu perro contentos con comida natural y fresca, rica en nutrientes, sin harinas y sin conservadores.</p>
        </div>
    </div>

    <div class="row">
        <div>Crear un plan</div>
    </div>


{/if}
</div>
</div>