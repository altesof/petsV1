{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div id="aei_cmsservice" class="block">
<div class="container">
{if $aeicmsserviceinfos.text != ""}	
		{$aeicmsserviceinfos.text nofilter}	
{else}

    <h1>Selecciona un plan:</h1>
    <h5>Gánate 30% de descuento en el primer pedido.</h5>
    <div class="row aei-service-first-row">
        <div class="col-md-4 aei-service-item first">
            <h1>Mensual</h1>
            <p>Ahorra dinero y recibe toda la comida del mes de tu peludo en una sola entrega.</p>
        </div>
        <div class="col-md-4 aei-service-item second">
            <h1>Quincenal</h1>
            <p>Para quienes tienen menos espacio en el congelador. Recibe la comida de tu peludo cada 15 días.</p>
        </div>
        <div class="col-md-4 aei-service-item third">
            <h1>Semanal</h1>
            <p>Tienes poquísimo espacio en tu congelador? No te preocupes! Recibe la comida de tu peludo cada 7 días.</p>
        </div>
    </div>

    <div class="row aei-service-second-row">
        <div class="col-md-3 aei-service-item first">
            <h1>Miniatura</h1>
            <p>A partir de $11/día</p>
        </div>
        <div class="col-md-3 aei-service-item second">
            <h1>Pequeño</h1>
            <p>A partir de $19/día</p>
        </div>
        <div class="col-md-3 aei-service-item third">
            <h1>Mediano</h1>
            <p>A partir de $25/día</p>
        </div>
        <div class="col-md-3 aei-service-item fourth">
            <h1>Grande</h1>
            <p>A partir de $29/día</p>
        </div>
    </div>

{/if}
</div>
</div>