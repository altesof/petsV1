{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="aei_cmsbanner"> 
<div class="container">
<div class="row">
	{if $aeicmsbannerinfos.text != ""}
		{$aeicmsbannerinfos.text nofilter}
	{else}				
		<div class="aei_banner-slider">
			<ul class="banner_inner" id="aei_banner_inner">
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner1.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">60<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">New<a class="aei-title2" href="#">Toaster</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner2.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">40<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">Latest<a class="aei-title2" href="#">Stove</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner3.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">50<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">Coffee<a class="aei-title2" href="#">maker</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner4.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">20<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">Solar<a class="aei-title2" href="#">cooker</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner5.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">60<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">Best<a class="aei-title2" href="#">Firepot</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
				<li class="aei-banner-item">
					<a href="#" class="aei_cmsbannerimage"><img src="{$image_url}/banner6.jpg" alt="" /></a>
					<div class="aei-details">
						<div class="aei-desc">40<span class="dcnt">%<span class="offer">off</span></span></div>
						<div class="aei-title">Latest<a class="aei-title2" href="#">haybox</a></div>
						<a href="#" class="btn btn-primary">shop now</a>
					</div>
				</li>
			</ul>
			<div id="aeibannerarrows" class="arrows"></div>
		</div>
	{/if}
</div>	
</div>
</div>
