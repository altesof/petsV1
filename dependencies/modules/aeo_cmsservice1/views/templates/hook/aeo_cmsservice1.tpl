{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="special-line"></div>
<div id="aeo_cmsservice1" class="block bg section-bg fill bg-fill">
<div class="container">
{if $aeocmsserviceinfos1.text != ""}	
		{$aeocmsserviceinfos1.text nofilter}	
{else}

    <div class="row aeo-service-first-row">
        <div class="col-md-4 aeo-service-item first">
        </div>
        <div class="col-md-4 aeo-service-item second">
            <h1>Ingredientes de grado humano</h1>
            <p>Nuestro objetivo es la salud de tu mascota a partir de una alimentación fresca, balanceada y nutritiva, diseñada por veterinarios nutriólogos</p>
        </div>
        <div class="col-md-4 aeo-service-item third">
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Logo-SENASICA-copy.png" alt="" width="300" height="95" />
        </div>
    </div>
    <div class="row aeo-service-second-row">
        <div class="col-md-4 aeo-service-item first">
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Group-164.png" alt="" width="32" height="37" />
            <p>Superfoods como semillas de chía, blueberries y ácidos Omega 3</p>
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Group-164.png" alt="" width="32" height="30" />
            <p>Carnes de alta calidad</p>
        </div>
        <div class="col-md-4 aeo-service-item second">
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Component-23-%E2%80%93-3-300x300.png" alt="" width="478" height="478" />
        </div>
        <div class="col-md-4 aeo-service-item third">
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Group-164.png" alt="" width="40" height="40" />
            <p>Sin subproductos, sin conservadores, sin harinas y sin aditivos artificiales</p>
            <img src="http://localhost/prestashop/themes/pets/modules/aeo_cmsservice1/views/img/Group-164.png" alt="" width="48" height="28" />
            <p>Frutas y vegetales</p>
        </div>
    </div>

{/if}
</div>
</div>