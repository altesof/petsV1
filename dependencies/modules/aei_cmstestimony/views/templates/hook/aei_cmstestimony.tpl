{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="aei-cmstestimony">
<div class="container">
{if $aeicmstestimonyinfos.text != ""}
	{$aeicmstestimonyinfos.text nofilter}

{else}	
	<div id="aeicmstestimony" class="testimony-block">
		<div class="ax-title">
			<h3 class="title_block ax-product-title">{l s='what our client say' d="Modules.PsBlog.Shop"}</h3>
		</div>
		<ul id="aeitestimony-slider" class="aeitestimony-slider product_list">
			<li class="item">
				<div class="item cms_face">
					<div class="product_inner_cms">
						<div class="testmonial-image"><img alt="testmonial" title="testmonial" src="{$image_url}/person1.jpg" /></div>
						<div class="test-client">
							<div class="name"><a href="#">Will Smith</a></div>
							<div class="designation"><a title="Iphone Developer" href="#">Iphone Developer</a></div>
						</div>
					</div>	
					<div class="testimony-details">
						<div class="desc">
							<p> <i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum the industry's standard dummy text ever since the 1500s
galley of type and scrambled it to <i class="fa fa-quote-right" aria-hidden="true"></i></p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="item cms_face">
					<div class="product_inner_cms">
						<div class="testmonial-image"><img alt="testmonial" title="testmonial" src="{$image_url}/person2.jpg" /></div>
						<div class="test-client">
							<div class="name"><a href="#">Linda Howard</a></div>
							<div class="designation"><a title="Iphone Developer" href="#">Designer</a></div>
						</div>
					</div>	
					<div class="testimony-details">
						<div class="desc">
							<p><i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum the industry's standard dummy text ever since the 1500s
galley of type and scrambled it to <i class="fa fa-quote-right" aria-hidden="true"></i></p>
						</div>
					</div>
				</div>
			</li>
			<li class="item">
				<div class="item cms_face">
					<div class="product_inner_cms">
						<div class="testmonial-image"><img alt="testmonial" title="testmonial" src="{$image_url}/person3.jpg" /></div>
						<div class="test-client">
							<div class="name"><a href="#">Jr. Robert Downey</a></div>
							<div class="designation"><a title="Iphone Developer" href="#">Web Developer</a></div>
						</div>
					</div>	
					<div class="testimony-details">
						<div class="desc">
							<p><i class="fa fa-quote-left" aria-hidden="true"></i> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum the industry's standard dummy text ever since the 1500s
galley of type and scrambled it to <i class="fa fa-quote-right" aria-hidden="true"></i></p>
						</div>
					</div>
				</div>
			</li>
		</ul>
		<!--<div id="aeitestimonyarrows" class="arrows"></div>-->
	</div>
{/if}
</div>
</div>

