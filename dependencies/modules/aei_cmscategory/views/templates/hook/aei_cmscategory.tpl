{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2018 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div id="aei_cmscategory" class="parallax"  data-source-url="{$image_url}/category-bg.jpg">
<div class="container">
      {if $aeicmscategoryinfos.text != ""}
      {$aeicmscategoryinfos.text nofilter}
      {else}  		
		<div class="aei-cat-item">
			<div class="aei-cat-details">
				<div class="aei-cat-title"> Branded <span> Electronic </span> </div>
				<div class="aei-cat-desc"> Products </div>
				<div class="aei-cat-dscnt"> Get Up to <span>40%</span> off in digital zone </div>
				<a class="aei-cat-shop-btn" href="#">Shop Now</a>
			</div>	
		</div>
      {/if}
</div>
</div>